export default class Errors {
    /**
     * Create a new Errors instance.
     */
    constructor () {
        this.errors = [];
    }

    /**
     * Determine if an errors exists for the given field.
     *
     * @param {string} field
     */
    has (field) {
        let hasField = false;

        this.errors.forEach((error, index, array) => {
            if (error.field === field) {
                hasField = true;
            }
        });

        return hasField;
        // return this.errors.hasOwnProperty(field);
    }

    /**
     * Determine if we have any errors.
     */
    any () {
        return this.errors.length > 0;
    }

    /**
     * Retrieve the error message for a field.
     *
     * @param {string} field
     */
    get (field) {
        let matchingError = null;

        this.errors.forEach((error, index, array) => {
            if (error.field === field) {
                matchingError = error;
            }
        });

        return matchingError;
    }

    /**
     * Add new error
     *
     * @param {object} error
     */
    add (error) {
        this.errors.push(error);
    }

    /**
     * Clear one or all error fields.
     *
     * @param {string|null} field
     */
    clear (field) {
        if (field) {
            this.errors.forEach((error, index, array) => {
                if (error.field === field) {
                    this.errors.remove(error);
                }
            });

            return;
        }

        this.errors = [];
    }
}
