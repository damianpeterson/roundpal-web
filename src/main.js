import App from './App';
import Vue from 'vue';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.css';

Vue.use(VueMaterial);
Vue.material.registerTheme({
    app: {
        primary: {
            color: 'blue',
            hue: '900'
        },
        accent: {
            color: 'light-blue',
            hue: '500'
        },
        warn: {
            color: 'deep-orange',
            hue: '500'
        }
    }
});

Vue.material.setCurrentTheme('app');
Vue.material.inkRipple = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    render: h => h(App)
});
